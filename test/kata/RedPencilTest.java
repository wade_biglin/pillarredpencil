
package kata;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Red Pencil unit test skeleton
 * @author Wade
 */
public class RedPencilTest {
    private RedPencil redPencil;
    
    public RedPencilTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
      redPencil = new RedPencil();
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void redPencilIsTrueIfPriceIsReduced() {
        assertEquals(true, redPencil.red(5));
    }
    
    @Test
    public void onlyRedPencilReductionsBetweenFiveAndThirty() {
        assertEquals(false, redPencil.red(4));
        assertEquals(false, redPencil.red(31));
    }
}
