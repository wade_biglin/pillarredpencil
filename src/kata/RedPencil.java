
package kata;

/**
 * Determines whether an item is "red penciled" by the system
 * @author Wade
 */
public class RedPencil {
    public boolean red (int reduction){
        if (reduction > 30 || reduction <5)
            return false;
        else
            return true;
    }
}
